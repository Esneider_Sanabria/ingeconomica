function separarMiles(numero, numeroDecimales) { 
    if ((numero + " ") == " ") { 
        return ""; 
    } 
    var negativo = (numero<0); 
    if (negativo) { 
        numero = -numero; 
    } 

    var entero = Math.floor(numero) + ""; 

    var decimal = (numero - entero) + ""; 
    var decimalFormateado = ""; 
    var valorASumar = 0; 
    if (numeroDecimales) { 
        var baseDecimal = Math.round(rpad(decimal.substring(decimal.indexOf('.') + 1), "0", numeroDecimales*1 + 1)/10) + ""; 
        if (baseDecimal.length > numeroDecimales) { 
            baseDecimal = baseDecimal.substring(1); 
            valorASumar = 1; 
        } 
        decimalFormateado = "." + lpad(baseDecimal, "0", numeroDecimales); 
    } 
    entero = (entero*1 + valorASumar*1) + ""; 

    var enteroFormateado = ""; 
    var longitudPrimerGrupo = entero.length % 3; 
    if ((longitudPrimerGrupo == 0) && (entero.length > 0)) { 
        longitudPrimerGrupo = 3; 
    } 
    var enteroFormateado = entero.substring(0, longitudPrimerGrupo); 

    for (var i=longitudPrimerGrupo; i<entero.length; i += 3) { 
        enteroFormateado += "," + entero.substring(i, i + 3); 
    } 

    return (negativo ? "-":"") + enteroFormateado + decimalFormateado; 
} 
  

function lpad(que, conQue, aCuanto) { 
    var aRetornar = que; 
    while (aRetornar.length < aCuanto) { 
        aRetornar = ("" + conQue) + ("" + aRetornar); 
    } 
    var desde = aRetornar.length - aCuanto; 
    return (desde > 0) ? aRetornar.substring(desde) : aRetornar; 
} 
  

function rpad(que, conQue, aCuanto) { 
    var aRetornar = que; 
    while (aRetornar.length < aCuanto) { 
        aRetornar += "" + conQue; 
    } 
    return aRetornar.substring(0, aCuanto); 
}


function quitarFormato(elNumero)
{
    lengthOfString = elNumero.length;
    
    for (i=0;i<lengthOfString;i++){
	if (elNumero.charAt(i)==','){
	    
	    elNumero = elNumero.substring(0,i) + elNumero.substring(i+1,lengthOfString);
	    lengthOfString -= 1;
	}
    } //end for
	
    return elNumero;
	
} //end quitarFormato



	
