function validarEntradas(elemento)
{
	if (blanks(elemento)){
		alert("Por favor ingrese los valores en los campos!");
		return false;
	}
	
	if (!validNumber(elemento)){
		alert("Los valores que ha ingresado no son num�ricos!");
		return false;
	}
	
	if (!validarFormato(elemento)){
		alert("El formato del n�mero no es correcto! Separe con coma (,) los miles y con punto (.) los decimales.");
		return false;
	}
	
	return true;
}

function validarTasas(tasaUno,tasaDos)
{
	if (blanks(tasaUno)){
		if (blanks(tasaDos)){
			alert("Debe ingresar una de las tasas para realizar la conversi�n!");
			return false;
		} 
		
		if (!validNumber(tasaDos)){
			alert("Los valores que ha ingresado no son num�ricos!");
			return false;
		}
		
		if (!validarFormato(tasaDos)){
			alert("El formato del n�mero no es correcto! Separe con coma (,) los miles y con punto (.) los decimales.");
			return false;
		}
		
	} else{
		if (!blanks(tasaDos)){
			alert("Deje una de las tasas en blanco!");
			return false;
		}
		if (!validNumber(tasaUno)){
			alert("Los valores que ha ingresado no son num�ricos!");
			return false;
		}
		
		if (!validarFormato(tasaUno)){
			alert("El formato del n�mero no es correcto! Separe con coma (,) los miles y con punto (.) los decimales.");
			return false;
		}
	  }
	
	return true;
}


function ltrim(theString)
{
	var i=0;
	while (i<theString.length && theString.charAt(i)==' '){
		i++;
	}
	return theString.substring(i);
}


function rtrim(theString)
{
	var i=theString.length-1;
	while (i>0 && theString.charAt(i)==' '){
		i--;
	}
	return theString.substring(0,i+1);
}


function trim(theString)
{
	return ltrim(rtrim(theString));
}


function blanks(Infield)
{
	if (trim(Infield)==""){
		return true;
	}
	return false;
}


function validNumber(aNumber)
{
	lengthOfString = aNumber.length;
	for (i=0;i<lengthOfString;i++){
		if (aNumber.charAt(i) < '0' || aNumber.charAt(i) > '9'){
		    if (aNumber.charAt(i)!='.' && aNumber.charAt(i)!=','){
			return false;
		    }
		}
	} //end for
	
	return true;
	
} //end validNumber


/*Verifica que el n�mero ingresado solo tenga un punto decimal, 
puede tener tantas comas como quiera*/

function validarFormato(elNumero)
{
    var numPtos = 0;
    lengthOfString = elNumero.length;
        
    for (i=0;i<lengthOfString;i++){
    	if (elNumero.charAt(i)=='.'){
    		numPtos += 1;
    	}
    } //end for
    
    if (numPtos > 1){
    	return false;  //Formato incorrecto
    }
    
    return true;	
}