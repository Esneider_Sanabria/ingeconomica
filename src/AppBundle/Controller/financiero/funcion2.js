/*Convierte una tasa efectiva a una tasa nominal.*/
function convEfecToNom(tasa,capitalizacion,periodo)
{
	
	tasaNominal = Math.pow(1 + (tasa / 100), (Math.abs(capitalizacion) / 360)) - 1;
	
	if (periodo > 0){
		tasaNominal = tasaNominal * (360 / Math.abs(capitalizacion)) * 100;
	} else{
		tasaNominal = tasaNominal / (1 + tasaNominal);
		tasaNominal = tasaNominal * (360 / Math.abs(capitalizacion)) * 100;
	}
	
	return tasaNominal;
}

/*Convierte una tasa nominal a efectiva*/
function convNomToEfect(tasa,capitalizacion,periodo)
{
  tasaNominal = tasa/100;
  
  if (Math.abs(capitalizacion) == 180){
        Dias = 180;
        tasaNominal = tasaNominal / 2;
  } else if (Math.abs(capitalizacion) == 90){
        Dias = 90;
        tasaNominal = tasaNominal / 4;
  } else if (Math.abs(capitalizacion) == 60){
        Dias = 60;
        tasaNominal = tasaNominal / 6;
  } else if (Math.abs(capitalizacion) == 30){
  	Dias = 30;
        tasaNominal = tasaNominal / 12;
  } else{
        Dias = Math.abs(capitalizacion);
        tasaNominal = tasaNominal / (360 / Dias);
    }
    
   if (Dias != 0){
   	P = 360 / Dias;
   }
   
   if (periodo < 0){
   	tasaNominal = tasaNominal / (1 - tasaNominal);
   }
   
   tasaNominal = Math.pow(1 + tasaNominal, P) - 1;
   
   tasaEfectiva = tasaNominal * 100;
   
   return tasaEfectiva;
   
}//End function

/*Convierte un valor presente a un valor futuro*/
function convPresToFut(valorPresente,tasa,plazo,tipo,numDias)
{
	plazoEnDias = plazo;
	
	if (tipo[1].checked){
		plazoEnDias = plazo * 30;	
	} else if (tipo[2].checked){
		if (numDias[0].checked){
			plazoEnDias = plazo * 360;
		} else{
			plazoEnDias = plazo * 365;
		}
	}
	if (numDias[0].checked){
		valorFuturo = Math.round(Math.pow(1 + tasa,plazoEnDias / 360) * valorPresente);
	} else{
		valorFuturo = Math.round(Math.pow(1 + tasa,plazoEnDias / 365) * valorPresente);
	}

	return valorFuturo;
}

/*Convierte un valor futuro a un valor presente*/
function convFutToPres (valorFuturo,tasa,plazo,tipo,numDias)
{
	plazoEnDias = plazo;
	
	if (tipo[1].checked){
		plazoEnDias = plazo * 30;
	} else if (tipo[2].checked){
		if (numDias[0].checked){
			plazoEnDias = plazo * 360;
		} else{
			plazoEnDias = plazo * 365;
		}
	}
	
	if (numDias[0].checked){
		valorPresente = Math.round(Math.pow(1 + tasa, -(plazoEnDias / 360)) * valorFuturo);
	} else{
		valorPresente = Math.round(Math.pow(1 + tasa, -(plazoEnDias / 365)) * valorFuturo);
	}
	
	return valorPresente;
}

/*Encuentra el plazo, dados el valor presente y futuro y la tasa*/
function encontrarPlazo(valorFuturo,valorPresente,tasa,tipo,numDias)
{
	plazo = Math.log(valorFuturo/valorPresente) / Math.log(1+tasa);
	
	if (tipo[0].checked){
		if (numDias[0].checked){
			plazo = plazo * 360;
		} else{
			plazo = plazo * 365;
		}
	} else if (tipo[1].checked){
		plazo = plazo * 12;
	} 
		 
	return plazo;
}


/*Encuentra el interes, dados el valor presente y futuro y el periodo*/
function encontrarInteres(valorFuturo,valorPresente,plazo,tipo,numDias)
{
	var plazoEnDias;
	
	if (tipo[1].checked){
		plazoEnDias = plazo * 30;
	} else if(tipo[2].checked){
		if (numDias[0].checked){
			plazoEnDias = plazo * 360;
		} else{
			plazoEnDias = plazo * 365;
		}
	} else{
		plazoEnDias = plazo;
	}
	
	if (numDias[0].checked){
		tasa = (Math.pow(valorFuturo/valorPresente,1/(plazoEnDias/360)) - 1) * 100;
	} else{
		tasa = (Math.pow(valorFuturo/valorPresente,1/(plazoEnDias/365)) - 1) * 100;
	}
	
	return tasa;
}