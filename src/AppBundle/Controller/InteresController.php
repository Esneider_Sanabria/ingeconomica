<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InteresController extends Controller
{

    /**
     * @Route("/interes", name="index")
     */
    public function calculadoraInteresAction(Request $request){
        
        return $this->render('interes/interes.html.twig');

    }

    /**
     * @param Request $request
     * @Route("/calcularInteres", name="calcularInteres")
     * @return JsonResponse
     */
    public function calcularInteresAction(Request $request){
        $answer = $request->request->all();

        $capital    = $answer['capital'];
        $tasa       = ($answer['tasa']/100);
        $periodo    = $answer['periodo'];
        $saldo = 0;
        $contador = 0;

        for ($i = 0; $i < $periodo; $i++)
        {
            $valorCuota = ($capital) / ( (1 - (pow((1 + $tasa), -$periodo))) / $tasa);
            $interes    = $saldo * $tasa;

            if($i == 0){
                $interes    = ($capital * $tasa);
            }

            $abono          = ($valorCuota - $interes);
            $contador       = $contador + $abono;
            $saldo          = ($capital - $contador);
            if($i == $periodo-1){
                $saldo      = 0;
            }
                $obj[] = array(
                "interes" => $interes,
                "saldo" => $saldo,
                "abono" => $abono

            );
        }

        return new JsonResponse($obj);
    }

}
